# Truchas Extensions

[Truchas](https://gitlab.com/truchas/truchas-release) is open source software
developed by Los Alamos National Laboratory
(LANL) and collaborators for physics-based modeling and simulation of
manufacturing processes. Its core capabilities are primarily geared toward metal
casting, with capabilities for metal additive manufacturing under active
development. It is, however, a general multi-physics computational tool that is
useful for a wider range of applications.

Truchas includes coupled physics models for incompressible multi-material flow
with interface tracking, heat transfer, phase change, view factor thermal
radiation, species advection-diffusion, elastic/plastic mechanics with contact,
and electromagnetics.  It uses conventional 3-D unstructured finite element
computational meshes composed of mixed element types, and employs finite volume,
finite element, and mimetic finite difference discretizations.

This project provides extensions for
[SMTK](https://www.computationalmodelbuilder.org/smtk/) to import,
export and manipulate data related to Truchas simulation.

# License

The Truchas Extensions project is distributed under the OSI-approved BSD
3-clause License. See [License.txt][] for details.

[License.txt]: LICENSE.txt
