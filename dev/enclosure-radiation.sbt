<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <UniqueRoles>
    <Role ID="11"></Role>
    <Role ID="12"></Role>
    <Role ID="13"></Role>
  </UniqueRoles>
  <Definitions>
    <AttDef Type="er1" Label="Enclosure System" BaseType="" Version="0">
      <ItemDefinitions>
        <File Name="Enclosure File"></File>
        <Void Name="Skip Geometry Check" Optional="true" EnabledByDefault="false" AdvanceLevel="1"></Void>
        <Double Name="Ambient Temperature">
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="Error Tolerance">
          <DefaultValue>0.001</DefaultValue>
        </Double>
        <Group Name="surfaces" Label="Surfaces" Extensible="True" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="emissivity" Label="Emissivity" Version="0">
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
            <Component Name="side-sets" Label="Side Sets" Extensible="true" NumberOfRequiredValues="0" Role="11">
              <Accepts>
                <Resource Name="smtk::model::Resource" Filter="face"></Resource>
              </Accepts>
            </Component>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="er1b" Label="Enclosure System" BaseType="" Version="0">
      <ItemDefinitions>
        <File Name="Enclosure File"></File>
        <Void Name="Skip Geometry Check" Optional="true" EnabledByDefault="false" AdvanceLevel="1"></Void>
        <Double Name="Ambient Temperature">
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="Error Tolerance">
          <DefaultValue>0.001</DefaultValue>
        </Double>
        <Group Name="surfaces" Label="Surfaces" Extensible="True" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="emissivity" Label="Emissivity" Version="0">
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
            <Group Name="side sets" Label="Side Sets" Extensible="True" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Component Name="side-set" Label="Side Set" NumberOfRequiredValues="1" Role="12">
                  <Accepts>
                    <Resource Name="smtk::model::Resource" Filter="face"></Resource>
                  </Accepts>
                </Component>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="er2surface" Label="Enclosure Surface" Unique="true">
      <AssociationsDef Name="ER2Associations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="emissivity" Label="Emissivity" Version="0">
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="er2" Label="Enclosure System">
      <ItemDefinitions>
        <File Name="Enclosure File"></File>
        <Void Name="Skip Geometry Check" Optional="true" EnabledByDefault="false" AdvanceLevel="1"></Void>
        <Double Name="Ambient Temperature">
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="Error Tolerance">
          <DefaultValue>0.001</DefaultValue>
        </Double>
        <Group Name="Enclosure Surfaces" Extensible="true" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Component Name="Surface" NumberOfRequiredValues="1" Role="13">
              <Accepts>
                <Resource Name="smtk::attribute::Resource" Filter="attribute[type='er2surface']"></Resource>
              </Accepts>
            </Component>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="er2b" Label="Enclosure System" Unique="true">
      <AssociationsDef Name="er2bAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Accepts>
          <Resource Name="smtk::attribute::Resource" Filter="attribute[type='er2surface']"></Resource>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <File Name="Enclosure File"></File>
        <Void Name="Skip Geometry Check" Optional="true" EnabledByDefault="false" AdvanceLevel="1"></Void>
        <Double Name="Ambient Temperature">
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="Error Tolerance">
          <DefaultValue>0.001</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Group" Title="Enclosure Radiation" TopLevel="true" TabPosition="North" FilterByAdvanceLevel="true" FilterByCategory="false">
      <Views>
        <View Title="Option1"></View>
        <View Title="Option1B"></View>
        <View Title="Option2: Surface"></View>
        <View Title="Option2: System"></View>
        <View Title="Option2B: System"></View>
        <View Title="2B-Tiled"></View>
        <View Title="2B-Tabbed"></View>
        <View Title="2B-GroupBox"></View>
      </Views>
    </View>
    <View Type="Attribute" Title="Option1">
      <AttributeTypes>
        <Att Type="er1"></Att>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Option1B">
      <AttributeTypes>
        <Att Type="er1b"></Att>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Option2: Surface">
      <AttributeTypes>
        <Att Type="er2surface"></Att>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Option2: System">
      <AttributeTypes>
        <Att Type="er2"></Att>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Option2B: System">
      <AttributeTypes>
        <Att Type="er2b"></Att>
      </AttributeTypes>
    </View>
    <View Type="Group" Title="2B-Tiled" Style="Tiled">
      <Views>
        <View Title="Option2: Surface"></View>
        <View Title="Option2B: System"></View>
      </Views>
    </View>
    <View Type="Group" Title="2B-Tabbed" Style="Tabbed" TabPosition="North">
      <Views>
        <View Title="Option2: Surface"></View>
        <View Title="Option2B: System"></View>
      </Views>
    </View>
    <View Type="Group" Title="2B-GroupBox" Style="GroupBox">
      <Views>
        <View Title="Option2: Surface"></View>
        <View Title="Option2B: System"></View>
      </Views>
    </View>
  </Views>
</SMTK_AttributeResource>
