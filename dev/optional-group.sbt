<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="test" BaseType="" Version="0">
      <ItemDefinitions>
        <Group Name="fluid" Label="Fluid" Optional="true" IsEnabledByDefault="false">
          <ItemDefinitions>
            <Double Name="viscosity" Label="Viscosity (nu)">
              <BriefDescription>The dynamic viscosity of a fluid phase</BriefDescription>
              <!-- <ExpressionType>fn.material.viscosity</ExpressionType> -->
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Instanced" Title="Test Optional Group" TopLevel="true" FilterByAdvanceLevel="false" FilterByCategory="false">
      <InstancedAttributes>
        <Att Name="test" Type="test"></Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
