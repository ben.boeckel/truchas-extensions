list(APPEND CMAKE_MODULE_PATH "${VTK_MODULES_DIR}")

set(ui_files smtkTruchasCoilsView.ui)
set(header_files
  pqSMTKDisplayToolBar.h
  pqSMTKTruchasExtensionsAutoStart.h
  qtTruchasViewRegistrar.h
  smtkTruchasCoilsView.h
  smtkTruchasMaterialsView.h)

set(view_source_files
  smtkTruchasCoilsView.cxx
  smtkTruchasMaterialsView.cxx)
set(qt_source_files
  qtTruchasViewRegistrar.cxx
  pqSMTKDisplayToolBar.cxx
  pqSMTKTruchasExtensionsAutoStart.cxx
  )

set(CMAKE_AUTOMOC 1)
set(CMAKE_AUTOUIC 1)

paraview_plugin_add_auto_start(
  CLASS_NAME pqSMTKTruchasExtensionsAutoStart
  INTERFACES auto_start_interfaces
  SOURCES auto_start_sources
  )

paraview_plugin_add_toolbar(
    CLASS_NAME pqSMTKDisplayToolBar
    INTERFACES toolbar_interfaces
    SOURCES toolbar_sources
  )

set(interfaces)
list(APPEND interfaces
    ${auto_start_interfaces}
    ${toolbar_interfaces}
  )

set(interface_sources)
list(APPEND interface_sources
  ${auto_start_sources}
  ${toolbar_sources}
  )

paraview_add_plugin(smtkTruchasPlugin
  VERSION "1.0"
  UI_INTERFACES
    ${interfaces}
  SOURCES
    ${header_files}
    ${view_source_files}
    ${qt_source_files}
    ${ui_files}
    ${interface_sources}
  UI_RESOURCES
    resources/pqSMTKDisplayToolBar.qrc
)

target_link_libraries(smtkTruchasPlugin
  LINK_PUBLIC
    smtkCore
    smtkQtExt
    smtkTruchas
    smtkPQComponentsExt
    smtkPVServerExt
    smtkTruchasQtExt
    ParaView::pqApplicationComponents
    ParaView::pqComponents
    ParaView::RemotingViews
    Qt5::Core
    Qt5::Widgets
)

target_include_directories(smtkTruchasPlugin PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  ${Boost_INCLUDE_DIRS}
)
