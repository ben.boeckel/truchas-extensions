//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/truchas/plugin/pqSMTKTruchasExtensionsAutoStart.h"

#include "pqActiveObjects.h"
#include "pqServer.h"

#include "smtk/simulation/truchas/plugin/qtTruchasViewRegistrar.h"
#include "smtk/simulation/truchas/plugin/smtkTruchasCoilsView.h"
#include "smtk/simulation/truchas/plugin/smtkTruchasMaterialsView.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtSMTKUtilities.h"

pqSMTKTruchasExtensionsAutoStart::pqSMTKTruchasExtensionsAutoStart(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKTruchasExtensionsAutoStart::~pqSMTKTruchasExtensionsAutoStart()
{
}

void pqSMTKTruchasExtensionsAutoStart::startup()
{
  // Check for current/active pqServer
  pqServer* server = pqActiveObjects::instance().activeServer();
  if (server != nullptr)
  {
    pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
    this->resourceManagerAdded(wrapper, server);
  }

  // Listen for server connections
  auto smtkBehavior = pqSMTKBehavior::instance();
  QObject::connect(smtkBehavior, static_cast<void (pqSMTKBehavior::*)(pqSMTKWrapper*, pqServer*)>(
                                   &pqSMTKBehavior::addedManagerOnServer),
    this, &pqSMTKTruchasExtensionsAutoStart::resourceManagerAdded);
  QObject::connect(smtkBehavior, static_cast<void (pqSMTKBehavior::*)(pqSMTKWrapper*, pqServer*)>(
                                   &pqSMTKBehavior::removingManagerFromServer),
    this, &pqSMTKTruchasExtensionsAutoStart::resourceManagerRemoved);
}

void pqSMTKTruchasExtensionsAutoStart::shutdown()
{
}

void pqSMTKTruchasExtensionsAutoStart::resourceManagerAdded(
  pqSMTKWrapper* wrapper, pqServer* server)
{
  if (!wrapper || !server)
  {
    return;
  }

  smtk::resource::ManagerPtr resourceManager = wrapper->smtkResourceManager();
  smtk::view::ManagerPtr viewManager = wrapper->smtkViewManager();
  if (!resourceManager || !viewManager)
  {
    return;
  }
  smtk::extension::qtTruchasViewRegistrar::registerTo(viewManager);
}

void pqSMTKTruchasExtensionsAutoStart::resourceManagerRemoved(pqSMTKWrapper* mgr, pqServer* server)
{
}
