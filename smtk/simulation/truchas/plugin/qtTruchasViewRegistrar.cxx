//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/truchas/plugin/qtTruchasViewRegistrar.h"

#include "smtk/simulation/truchas/plugin/smtkTruchasCoilsView.h"
#include "smtk/simulation/truchas/plugin/smtkTruchasMaterialsView.h"

#include <iostream>
#include <map>
#include <tuple>

namespace smtk
{
namespace extension
{
namespace
{
typedef std::tuple<smtkTruchasCoilsView, smtkTruchasMaterialsView> ViewWidgetList;
}

void qtTruchasViewRegistrar::registerTo(const smtk::view::Manager::Ptr& viewManager)
{
  bool ret = viewManager->viewWidgetFactory().registerTypes<ViewWidgetList>();
}

void qtTruchasViewRegistrar::unregisterFrom(const smtk::view::Manager::Ptr& viewManager)
{
  viewManager->viewWidgetFactory().unregisterTypes<ViewWidgetList>();
}
}
}
