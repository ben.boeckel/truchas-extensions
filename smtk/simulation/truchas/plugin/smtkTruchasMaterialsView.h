//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME smtkTruchasMaterialsView.h - SMTK view for Truchas Materials

#ifndef __smtk_simulation_truchas_qt_smtkTruchasMaterialsView_h
#define __smtk_simulation_truchas_qt_smtkTruchasMaterialsView_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/SharedFromThis.h"
#include "smtk/extension/qt/qtBaseAttributeView.h"
#include "smtk/extension/qt/qtBaseView.h"

class smtkTruchasMaterialsViewInternals;

namespace smtk
{
namespace extension
{
class qtItem;
}
}
class QTableWidgetItem;

/* **
  Provides a widget encapsulating the custom editor for Truchas
  material attributes. Includes an interleaved display of phase
  and transition items, each as a collapsible widget.
 */

typedef smtk::extension::qtBaseView qtBaseView;
using smtk::extension::qtBaseAttributeView;

class smtkTruchasMaterialsView : public qtBaseAttributeView
{
  Q_OBJECT

public:
  smtkTypenameMacro(smtkTruchasMaterialsView);

  static qtBaseView* createViewWidget(const smtk::view::Information& info);
  smtkTruchasMaterialsView(const smtk::view::Information& info);
  virtual ~smtkTruchasMaterialsView();

  bool isEmpty() const override;

public slots:
  void loadFromSbiFile();
  void onShowCategory() override;
  void saveToSbiFile();
  void updateModelAssociation() override;
  void updateUI() override;

signals:
  void numOfAttributesChanged();

protected:
  void createWidget() override;

protected slots:
  void onAttributeChanged();
  void onAttributeSelectionChanged(smtk::attribute::AttributePtr);
  void onItemChanged(smtk::extension::qtItem*);

private:
  smtkTruchasMaterialsViewInternals* Internals;
};

#endif // __smtk_simulation_truchas_qt_smtkTruchasMaterialsView_h
