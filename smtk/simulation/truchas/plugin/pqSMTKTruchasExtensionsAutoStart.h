//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_truchas_plugin_pqSMTKTruchasExtensionsAutoStart_h
#define smtk_simulation_truchas_plugin_pqSMTKTruchasExtensionsAutoStart_h

#include <QObject>

class pqServer;
class pqSMTKWrapper;

class pqSMTKTruchasExtensionsAutoStart : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqSMTKTruchasExtensionsAutoStart(QObject* parent = nullptr);
  ~pqSMTKTruchasExtensionsAutoStart() override;

  void startup();
  void shutdown();

protected slots:
  void resourceManagerAdded(pqSMTKWrapper* mgr, pqServer* server);
  void resourceManagerRemoved(pqSMTKWrapper* mgr, pqServer* server);

private:
  Q_DISABLE_COPY(pqSMTKTruchasExtensionsAutoStart);
};

#endif
