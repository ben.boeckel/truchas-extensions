//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtkTruchasMaterialsView.h"

// Truchas extension includes
#include "smtk/simulation/truchas/qt/qtAttributeListWidget.h"
#include "smtk/simulation/truchas/qt/qtMaterialAttribute.h"
#include "smtk/simulation/truchas/qt/qtValidators.h"

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/operators/Export.h"
#include "smtk/attribute/operators/Import.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtAttribute.h"
#include "smtk/extension/qt/qtItem.h"
#include "smtk/extension/qt/qtTableWidget.h"
#include "smtk/extension/qt/qtTypeDeclarations.h"
#include "smtk/extension/qt/qtUIManager.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/view/Configuration.h"

// ParaView client
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqServer.h"
#include "vtkSMProperty.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMStringVectorProperty.h"

#include <QDebug>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QMessageBox>
#include <QPointer>
#include <QPushButton>
#include <QSizePolicy>
#include <QSplitter>
#include <QString>
#include <QVBoxLayout>
#include <QVariant>

#include <set>
#include <string>
#include <vector>

class smtkTruchasMaterialsViewInternals
{
public:
  // Internal member data
  qtAttributeListWidget* ListFrame; // upper panel displaying list of attributes
  QPointer<QFrame> EditorFrame;     // lower panel displaying attribute editor
  QPointer<qtMaterialAttribute> CurrentAtt;

  smtk::view::Configuration::Component AttComponent;
  std::string AttributeType;
}; // Internals

qtBaseView* smtkTruchasMaterialsView::createViewWidget(const smtk::view::Information& info)
{
  smtkTruchasMaterialsView* view = new smtkTruchasMaterialsView(info);
  view->buildUI();
  return view;
}

smtkTruchasMaterialsView::smtkTruchasMaterialsView(const smtk::view::Information& info)
  : qtBaseAttributeView(info)
{
  this->Internals = new smtkTruchasMaterialsViewInternals();

  smtk::view::ConfigurationPtr view = this->getObject();
  int typesIndex = view->details().findChild("AttributeTypes");
  if (typesIndex < 0)
  {
    qWarning() << "ERROR: smtkTruchasMaterialsView missing AttributeTypes element";
    return;
  }
  smtk::view::Configuration::Component typesComp = m_viewInfo.m_view->details().child(typesIndex);
  std::size_t i, n = typesComp.numberOfChildren();
  for (i = 0; i < n; i++)
  {
    smtk::view::Configuration::Component& attComp = typesComp.child(i);
    if (attComp.name() == "Att")
    {
      this->Internals->AttComponent = attComp;
      break;
    }
  } // for (i)

  // Check that attribute Type is specified
  if (!this->Internals->AttComponent.attribute("Type", this->Internals->AttributeType))
  {
    qWarning()
      << "ERROR: smtkTruchasMaterialsView/AttributeTypes missing Att element or Type attribute";
  }
}

smtkTruchasMaterialsView::~smtkTruchasMaterialsView()
{
  delete this->Internals;
}

bool smtkTruchasMaterialsView::isEmpty() const
{
  // Returns true when categories are *not* enabled

  // Sanity check
  if (this->Internals->AttributeType.empty())
  {
    qWarning() << "Missing material attribute type in view specification";
    return true;
  }

  auto attResource = this->uiManager()->attResource();
  auto attDef = attResource->findDefinition(this->Internals->AttributeType);
  auto& categories = attDef->categories();
  bool pass = this->uiManager()->passCategoryCheck(categories);
  return !pass;
}

void smtkTruchasMaterialsView::loadFromSbiFile()
{
  // Get the smtk operation manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // Get workspace folder from smtk settings
  std::string projectsRootFolder;
  vtkSMProxy* proxy = server->proxyManager()->GetProxy("settings", "SMTKSettings");
  if (proxy)
  {
    vtkSMProperty* prop = proxy->GetProperty("ProjectsRootFolder");
    auto stringProp = vtkSMStringVectorProperty::SafeDownCast(prop);
    if (stringProp)
    {
      projectsRootFolder = stringProp->GetElement(0);
    } // if (stringProp)
  }   // if (proxy)

  // Get the input filepath
  QString filter("sbi files (*.sbi);;All files (*.*)");
  pqFileDialog fileDialog(server, pqCoreUtilities::mainWidget(), tr("Select File to Import"),
    QString::fromStdString(projectsRootFolder), filter);
  fileDialog.setObjectName("FileLoadDialog");
  fileDialog.setFileMode(pqFileDialog::ExistingFile);
  if (fileDialog.exec() != QDialog::Accepted)
  {
    return;
  }
  QString filePath = fileDialog.getSelectedFiles()[0];

  // Setup import operation
  smtk::operation::OperationPtr importOp = opManager->create<smtk::attribute::Import>();
  if (!importOp)
  {
    QString msg("Could not create \"import attribute operation\"");
    smtkErrorMacro(smtk::io::Logger::instance(), msg.toStdString());
    QMessageBox::critical(pqCoreUtilities::mainWidget(), "ERROR reading attributes", msg);
    return;
  }

  std::shared_ptr<smtk::attribute::Attribute> paramsAtt = importOp->parameters();

  auto fileItem = paramsAtt->findFile("filename");
  fileItem->setValue(filePath.toStdString());

  // Set option to load sbi file into current resource
  auto resourceItem = paramsAtt->findResource("use-resource");
  if (resourceItem == nullptr)
  {
    QString msg("Did not find \"use-resource\" item");
    smtkErrorMacro(smtk::io::Logger::instance(), msg.toStdString());
    QMessageBox::critical(pqCoreUtilities::mainWidget(), "ERROR reading attributes", msg);
    return;
  }
  resourceItem->setIsEnabled(true);
  auto attResource = this->uiManager()->attResource();
  resourceItem->setValue(attResource);

  QString msg;
  QTextStream qs(&msg);
  try
  {
    auto result = importOp->operate();
    int outcome = result->findInt("outcome")->value();
    if (outcome == static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      qs << "Loaded " << filePath;
      QMessageBox::information(pqCoreUtilities::mainWidget(), "Success", msg);
    }
    else
    {
      qs << "\"import attribute operation\" failed, outcome " << outcome;
      QMessageBox::critical(pqCoreUtilities::mainWidget(), "FAILED", msg);
      smtkErrorMacro(importOp->log(), msg.toStdString());
    }
  }
  catch (const std::exception& e)
  {
    QMessageBox::critical(pqCoreUtilities::mainWidget(), "ERROR", e.what());
    smtkErrorMacro(importOp->log(), e.what());
  }

  this->onShowCategory();
}

void smtkTruchasMaterialsView::saveToSbiFile()
{
  // Get the smtk operation manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // Get workspace folder from smtk settings
  std::string projectsRootFolder;
  vtkSMProxy* proxy = server->proxyManager()->GetProxy("settings", "SMTKSettings");
  if (proxy)
  {
    vtkSMProperty* prop = proxy->GetProperty("ProjectsRootFolder");
    auto stringProp = vtkSMStringVectorProperty::SafeDownCast(prop);
    if (stringProp)
    {
      projectsRootFolder = stringProp->GetElement(0);
    } // if (stringProp)
  }   // if (proxy)

  // Get output filepath
  QString filter("sbi files (*.sbi);;All files (*.*)");
  pqFileDialog fileDialog(server, pqCoreUtilities::mainWidget(), tr("Select File to Save"),
    QString::fromStdString(projectsRootFolder), filter);
  fileDialog.setObjectName("FileSaveDialog");
  fileDialog.setFileMode(pqFileDialog::AnyFile);
  if (fileDialog.exec() != QDialog::Accepted)
  {
    return;
  }
  QString filePath = fileDialog.getSelectedFiles()[0];

  // Setup export operation
  smtk::operation::OperationPtr exportOp = opManager->create<smtk::attribute::Export>();
  if (!exportOp)
  {
    QString msg("Could not create \"export attribute operation\"");
    smtkErrorMacro(smtk::io::Logger::instance(), msg.toStdString());
    QMessageBox::critical(pqCoreUtilities::mainWidget(), "ERROR writing attributes", msg);
    return;
  }

  auto attResource = this->uiManager()->attResource();
  std::shared_ptr<smtk::attribute::Attribute> paramsAtt = exportOp->parameters();
  paramsAtt->associate(attResource);
  auto fileItem = paramsAtt->findFile("filename");
  fileItem->setValue(filePath.toStdString());

  // Set option to only write material attributes and functions
  auto groupItem = exportOp->parameters()->findGroup("attribute-collection");
  if (groupItem == nullptr)
  {
    QString msg("Did not find \"attribute-collection\" item");
    smtkErrorMacro(smtk::io::Logger::instance(), msg.toStdString());
    QMessageBox::critical(pqCoreUtilities::mainWidget(), "ERROR reading attributes", msg);
    return;
  }
  groupItem->setIsEnabled(true);
  auto typesItem = groupItem->findAs<smtk::attribute::StringItem>("types");
  if (typesItem == nullptr)
  {
    QString msg("Did not find \"attribute-collection/types\" item");
    smtkErrorMacro(smtk::io::Logger::instance(), msg.toStdString());
    QMessageBox::critical(pqCoreUtilities::mainWidget(), "ERROR reading attributes", msg);
    return;
  }
  typesItem->setNumberOfValues(2);
  typesItem->setValue(0, "material.real");
  typesItem->setValue(1, "fn.material");

  QString msg;
  QTextStream qs(&msg);
  try
  {
    auto result = exportOp->operate();
    int outcome = result->findInt("outcome")->value();
    if (outcome == static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      qs << "Wrote " << filePath;
      QMessageBox::information(pqCoreUtilities::mainWidget(), "Success", msg);
    }
    else
    {
      qs << "\"export attribute operation\" failed, outcome " << outcome;
      QMessageBox::critical(pqCoreUtilities::mainWidget(), "FAILED", msg);
      smtkErrorMacro(exportOp->log(), msg.toStdString());
    }
  }
  catch (const std::exception& e)
  {
    QMessageBox::critical(pqCoreUtilities::mainWidget(), "ERROR", e.what());
    smtkErrorMacro(exportOp->log(), e.what());
  }
}

void smtkTruchasMaterialsView::onShowCategory()
{
  this->updateUI();
}

void smtkTruchasMaterialsView::updateModelAssociation()
{
  this->updateUI();
}

void smtkTruchasMaterialsView::updateUI()
{
  // Call ListFrame's onShowCategory() to trigger this event chain:
  //   - ListFrame re-selects the current attribute (if any)
  //   - ListFrame emits onAttributeSelectionChanged signal
  //   - signal is connected to smtkTruchasMaterials::onAttributeSelectionChanged()
  //   - which redraws the EditorFrame
  this->Internals->ListFrame->onShowCategory();
}

void smtkTruchasMaterialsView::createWidget()
{
  // Display a simplified version of qtAttributeView top frame
  auto attResource = this->uiManager()->attResource();
  auto materialDefinition = attResource->findDefinition(this->Internals->AttributeType);

  QSplitter* frame = new QSplitter(this->parentWidget());
  frame->setOrientation(Qt::Vertical);

  this->Internals->ListFrame =
    new qtAttributeListWidget(frame, this, materialDefinition, isAttributeValid);
  // this->Internals->ListFrame->setAttributeValidator(isAttributeValid);
  this->Internals->ListFrame->showLoadSaveButtons(true);
  QObject::connect(this->Internals->ListFrame, &qtAttributeListWidget::loadButtonClicked, this,
    &smtkTruchasMaterialsView::loadFromSbiFile);
  QObject::connect(this->Internals->ListFrame, &qtAttributeListWidget::saveButtonClicked, this,
    &smtkTruchasMaterialsView::saveToSbiFile);

  // Attribute frame
  this->Internals->EditorFrame = new QFrame(frame);
  new QVBoxLayout(this->Internals->EditorFrame);
  //  this->Internals->EditorFrame->setVisible(0);

  frame->addWidget(this->Internals->ListFrame);
  frame->addWidget(this->Internals->EditorFrame);

  QObject::connect(this->Internals->ListFrame, &qtAttributeListWidget::attributeCreated, this,
    &qtBaseAttributeView::attributeCreated);
  QObject::connect(this->Internals->ListFrame, &qtAttributeListWidget::attributeRemoved, this,
    &qtBaseAttributeView::attributeRemoved);

  // We want the signals that may change the attribute to be displayed Queued instead of
  // Direct so that QLineEdit::editingFinished signals are processed prior to these.
  QObject::connect(this->Internals->ListFrame, &qtAttributeListWidget::attributeSelectionChanged,
    this, &smtkTruchasMaterialsView::onAttributeSelectionChanged, Qt::QueuedConnection);
  QObject::connect(this->Internals->ListFrame, &qtAttributeListWidget::attributeNameChanged, this,
    &qtBaseAttributeView::attributeChanged, Qt::QueuedConnection);

  this->Widget = frame;
} // createWidget()

void smtkTruchasMaterialsView::onAttributeChanged()
{
  auto* qAtt = dynamic_cast<qtMaterialAttribute*>(this->sender());
  assert(qAtt);
  auto att = qAtt->attribute();
  this->Internals->ListFrame->checkStatus(att);
}

void smtkTruchasMaterialsView::onAttributeSelectionChanged(smtk::attribute::AttributePtr att)
{
  this->Internals->EditorFrame->setVisible(true);
  if (this->Internals->CurrentAtt != nullptr)
  {
    this->Internals->EditorFrame->layout()->removeWidget(this->Internals->CurrentAtt->widget());
  }
  delete this->Internals->CurrentAtt;
  this->Internals->CurrentAtt = nullptr;

  if (att != nullptr)
  {
    this->Internals->CurrentAtt = new qtMaterialAttribute(
      att, this->Internals->AttComponent, this->Internals->EditorFrame, this);

    this->Internals->CurrentAtt->createBasicLayout(false);
    if (this->Internals->CurrentAtt->widget())
    {
      this->Internals->EditorFrame->layout()->addWidget(this->Internals->CurrentAtt->widget());
      if (this->advanceLevelVisible())
      {
        this->Internals->CurrentAtt->showAdvanceLevelOverlay(true);
      }
    }

    QObject::connect(this->Internals->CurrentAtt, &qtMaterialAttribute::itemModified, this,
      &smtkTruchasMaterialsView::onItemChanged, Qt::QueuedConnection);
    QObject::connect(this->Internals->CurrentAtt, &qtMaterialAttribute::modified, this,
      &smtkTruchasMaterialsView::onAttributeChanged, Qt::QueuedConnection);
  } // if (att)
}

void smtkTruchasMaterialsView::onItemChanged(smtk::extension::qtItem* qitem)
{
  // qDebug() << "onItemChanged";
  if (qitem == nullptr)
  {
    return;
  }
  auto item = qitem->item();
  if (item == nullptr)
  {
    return;
  }
  auto attPtr = item->attribute();
  if (attPtr == nullptr)
  {
    return;
  }

  this->Internals->ListFrame->checkStatus(attPtr);
  this->valueChanged(item);
  std::vector<std::string> itemNames{ item->name() };
  this->attributeChanged(attPtr, itemNames);
}
