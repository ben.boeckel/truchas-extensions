set(qt_sources
  ctkCollapsibleButton.cpp
  qtAttributeListWidget.cxx
  qtDragSelfFilter.cxx
  qtInductionCoilsModel.cxx
  qtInductionSourceModel.cxx
  qtMaterialAttribute.cxx
  qtMaterialItem.cxx
  qtMaterialMultiPhaseMoveButtons.cxx
  qtSharedPropertiesItem.cxx
  )

set(moc_headers
  ctkCollapsibleButton.h
  qtAttributeListWidget.h
  qtDragSelfFilter.h
  qtInductionCoilsModel.h
  qtInductionSourceModel.h
  qtMaterialAttribute.h
  qtMaterialItem.h
  qtMaterialMultiPhaseMoveButtons.h
  qtSharedPropertiesItem.h
  )

set(qt_headers
  qtValidators.h
  ${moc_headers}
  )

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)

add_library(smtkTruchasQtExt
  ${qt_sources}
  ${MOC_BUILT_SOURCES}
)

# Add location of moc files
target_include_directories(smtkTruchasQtExt PRIVATE ${CMAKE_CURRENT_BINARY_DIR})
target_include_directories(smtkTruchasQtExt
  PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
    $<INSTALL_INTERFACE:include>
  PRIVATE
    ${Boost_INCLUDE_DIRS}
)

# Publicly link to smtkCore
target_link_libraries(smtkTruchasQtExt
  LINK_PUBLIC
    smtkTruchas
    smtkQtExt
    smtkCore
    Qt5::Core
    Qt5::Widgets
  )
smtk_export_header(smtkTruchasQtExt Exports.h)

smtk_install_library(smtkTruchasQtExt)
