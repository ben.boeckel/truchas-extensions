#include "qtMaterialMultiPhaseMoveButtons.h"

qtMaterialMultiPhaseMoveButtons::qtMaterialMultiPhaseMoveButtons(
  QWidget* parent, int row, bool is_lastRow)
  : Superclass(parent)
  , m_row(row)
  , m_isLastRow(is_lastRow)
{
  m_isPhase = row % 2 == 0;
  m_layout = new QHBoxLayout();
  this->setLayout(m_layout);

  m_upButton = new QPushButton;
  m_downButton = new QPushButton;
  QCommonStyle style;
  m_upButton->setIcon(style.standardIcon(QStyle::SP_ArrowUp));
  m_downButton->setIcon(style.standardIcon(QStyle::SP_ArrowDown));

  m_layout->addWidget(m_upButton);
  m_layout->addWidget(m_downButton);

  this->setUpButtonVisibility(m_row && m_isPhase);
  this->setDownButtonVisibility(!m_isLastRow && m_isPhase);

  QObject::connect(
    m_upButton, &QPushButton::clicked, this, &qtMaterialMultiPhaseMoveButtons::upPushed);
  QObject::connect(
    m_downButton, &QPushButton::clicked, this, &qtMaterialMultiPhaseMoveButtons::downPushed);
}

void qtMaterialMultiPhaseMoveButtons::upPushed()
{
  emit moveUp(m_row);
}

void qtMaterialMultiPhaseMoveButtons::downPushed()
{
  emit moveDown(m_row);
}

void qtMaterialMultiPhaseMoveButtons::setLastRow(bool lastRow)
{
  this->m_isLastRow = lastRow;

  this->setDownButtonVisibility(!m_isLastRow && m_isPhase);
}

void qtMaterialMultiPhaseMoveButtons::setRow(int row)
{
  this->m_row = row;
  this->m_isPhase = row % 2 == 0;

  this->setUpButtonVisibility(m_row && m_isPhase);
  this->setDownButtonVisibility(!m_isLastRow && m_isPhase);
}

void qtMaterialMultiPhaseMoveButtons::setUpButtonVisibility(bool show)
{
  this->setButtonVisibility(this->m_upButton, show);
}

void qtMaterialMultiPhaseMoveButtons::setDownButtonVisibility(bool show)
{
  this->setButtonVisibility(this->m_downButton, show);
}

void qtMaterialMultiPhaseMoveButtons::setButtonVisibility(QPushButton* button, bool show)
{
  button->setEnabled(show);
  button->setVisible(show);
}
