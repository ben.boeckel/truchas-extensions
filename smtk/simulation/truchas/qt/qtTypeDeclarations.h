//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_simulation_truchas_qt_qtTypeDeclarations_h
#define __smtk_simulation_truchas_qt_qtTypeDeclarations_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/attribute/Attribute.h"

#include <QMetaType>

#include <string>
#include <vector>

// Allow QVariant objects to hold shared pointers
// for signal-slot connections.
Q_DECLARE_METATYPE(smtk::attribute::Attribute::Ptr)
Q_DECLARE_METATYPE(std::vector<std::string>)

#endif // __smtk_simulation_truchas_qt_qtTypeDeclarations_h
