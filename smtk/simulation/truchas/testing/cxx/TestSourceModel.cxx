//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/io/AttributeReader.h"
#include "smtk/io/Logger.h"

#include "smtk/simulation/truchas/qt/qtInductionSourceModel.h"

#include "helpers.h"

#include <QDir>
#include <QString>

namespace
{
QDir dataRoot(DATA_DIR);
}

int TestSourceModel(int, char**)
{
  // Load attribute file
  std::string attFile = dataRoot.absoluteFilePath("induction-heating.sbi").toStdString();
  auto attResource = smtk::attribute::Resource::create();
  auto attReader = smtk::io::AttributeReader();
  auto logger = smtk::io::Logger::instance();
  bool err = attReader.read(attResource, attFile, logger);
  smtkTest(!err, "Error reading attribute file" << attFile);

  // Use induction-heating attribute to initialize source model
  std::string attName = "induction-heating";
  auto att = attResource->findAttribute(attName);
  smtkTest(att != nullptr, "Error finding " << attName << " attribute");

  auto sourceModel = new qtInductionSourceModel(nullptr, att);
  test(sourceModel != nullptr, "Failed to create qtInductionSourceModel");

  /*
    The attribute file contains 2 coils and 3 time steps:
    time freq   coil1  coil2
     0.0  1000  500.5  999.9
    66.6  2000  600.6  888.8
    99.9  1500  700.7  777.7
  */

  int nrows = sourceModel->rowCount();
  smtkTest(nrows == 3, "Wrong number of rows, should be 3 not " << nrows);

  // Source table has 2 columns (time, frequency, coil 1)
  int ncols = sourceModel->columnCount();
  smtkTest(ncols == 4, "Wrong number of columns, should be 4 not " << ncols);

  // Check values

  QModelIndex index;
  QVariant variant;
  bool comp;
  bool ok;
  auto sourceItem = att->findGroup("source");
  auto coilsItem = att->findGroup("coils");

  // Time (column 0)
  index = sourceModel->index(1, 0);
  variant = sourceModel->data(index);
  test(variant.isValid(), "Variant not valid");
  comp = compareDouble(variant.toDouble(), 66.6);
  test(comp, "Time data at index (1,0) incorrect, should be 66.6");
  // Change value
  double pi = 3.14159;
  ok = sourceModel->setData(index, QVariant(pi));
  test(ok, "Failed to set time value");
  auto timeItem = sourceItem->findAs<smtk::attribute::DoubleItem>(1, "time");
  comp = compareDouble(timeItem->value(), pi);
  test(comp, "Time setData at index (1,0) incorrect; should be 3.14159");

  // Frequency (column 1)
  index = sourceModel->index(0, 1);
  variant = sourceModel->data(index);
  test(variant.isValid(), "Variant not valid");
  test(variant.toInt() == 1000, "Frequency data at index (0,1) incorrect, should be 1000");
  // Change value
  int ultimate = 42;
  ok = sourceModel->setData(index, ultimate);
  test(ok, "Failed to set frequency value");
  auto frequencyItem = sourceItem->findAs<smtk::attribute::IntItem>(0, "frequency");
  smtkTest(frequencyItem->value() == ultimate,
    "Frequency setData at index (0,1) incorrect; should be " << ultimate);

  // Coil 1 (column 2)
  index = sourceModel->index(1, 2);
  variant = sourceModel->data(index);
  test(variant.isValid(), "Variant not valid");
  comp = compareDouble(variant.toDouble(), 600.6);
  test(comp, "Current at index (1,2) incorrect, should be 600.6");
  // Change value
  double e = 2.71828;
  ok = sourceModel->setData(index, QVariant(e));
  test(ok, "Failed to set coil 1 current value");
  auto current1Item = coilsItem->findAs<smtk::attribute::DoubleItem>(0, "current");
  comp = compareDouble(current1Item->value(1), e);
  smtkTest(comp, "Coil 1 setData at index (1,2) incorrect; should be " << e << ", not "
                                                                       << current1Item->value());

  // Coil 2 (column 3)
  index = sourceModel->index(2, 3);
  variant = sourceModel->data(index);
  test(variant.isValid(), "Variant not valid");
  comp = compareDouble(variant.toDouble(), 2345.6);
  test(comp, "Current at index (2,3) incorrect, should be 2345.6");
  // Change value
  double M = 1000;
  ok = sourceModel->setData(index, QVariant(M));
  test(ok, "Failed to set coil 2 current value");
  auto current2Item = coilsItem->findAs<smtk::attribute::DoubleItem>(1, "current");
  comp = compareDouble(current2Item->value(2), M);
  smtkTest(comp, "Coil 2 setData at index (2,3) incorrect; should be " << M << ", not "
                                                                       << current2Item->value());

  return 0;
}
