set(smtkTruchasSimulationTests
    test_import
)

set(smtk_module_dir ${smtk_PREFIX_PATH}/${SMTK_PYTHONPATH})
if(NOT IS_ABSOLUTE ${smtk_module_dir})
  get_filename_component(smtk_module_dir
    ${PROJECT_BINARY_DIR}/${smtk_DIR}/${SMTK_PYTHON_MODULEDIR} ABSOLUTE)
endif()

set(pyenv
  ${PROJECT_BINARY_DIR}
  ${PROJECT_BINARY_DIR}/smtksimulationtruchas
  ${smtk_module_dir}
  $ENV{PYTHONPATH})
if (WIN32)
  string(REPLACE ";" "\;" pyenv "${pyenv}")
else ()
  string(REPLACE ";" ":" pyenv "${pyenv}")
endif()

set(pathenv)
if (WIN32)
  set(pathenv
    ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
    $ENV{PATH})
  string(REPLACE ";" "\;" pathenv "${pathenv}")
endif()

foreach (test ${smtkTruchasSimulationTests})
  add_test(
    NAME ${test}Py
    COMMAND  "${PYTHON_EXECUTABLE}" "${CMAKE_CURRENT_SOURCE_DIR}/${test}.py"
    --data-dir=${PROJECT_SOURCE_DIR}/data
    --temp-dir=${CMAKE_BINARY_DIR}/Testing/Temporary
    )
    set_tests_properties("${test}Py"
    PROPERTIES
    ENVIRONMENT "PYTHONPATH=${pyenv}"
    LABELS "Truchas Utility"
    )
  if (pathenv)
    set_property(TEST "${test}Py" APPEND
      PROPERTY
        ENVIRONMENT "PATH=${pathenv}"
      )
  endif ()
endforeach()
