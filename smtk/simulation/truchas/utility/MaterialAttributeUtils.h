//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_truchas_utility_MaterialAttributeUtils_h
#define smtk_simulation_truchas_utility_MaterialAttributeUtils_h

#include "smtk/simulation/truchas/Exports.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/attribute/Categories.h"

#include <string>

namespace smtk
{
namespace simulation
{
namespace truchas
{
class SMTKTRUCHAS_EXPORT MaterialAttributeUtils
{
public:
  MaterialAttributeUtils();

  // Methods that modify the attribute
  bool configureNewAttribute(smtk::attribute::AttributePtr att) const;
  bool configureCopiedAttribute(smtk::attribute::AttributePtr att) const;
  bool addPhase(smtk::attribute::AttributePtr att) const;
  bool removePhase(smtk::attribute::AttributePtr att, std::size_t phase) const;
  bool enableSharedItem(smtk::attribute::ItemPtr sharedItem, bool enabled) const;

  // Methods that check validity
  bool isValid(smtk::attribute::AttributePtr att) const;
  bool isValid(smtk::attribute::AttributePtr att, const std::set<std::string>& categories,
    std::string& reason) const;
  bool isPhaseValid(smtk::attribute::GroupItemPtr phaseGroup, std::size_t element,
    const std::set<std::string>& categories, std::string& reason) const;
  bool isTransitionValid(smtk::attribute::GroupItemPtr transitionsGroup, std::size_t element,
    double& minTemp, double& maxTemp, std::string& reason) const;

protected:
  bool isMonotonic(smtk::attribute::DoubleItemPtr item, bool& direction) const;

  // Phase item names that are never optional and, therefore, forceRequired can be skipped
  std::set<std::string> m_skipNames;
};
}
}
}

#endif
