//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/truchas/utility/MaterialAttributeUtils.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/ItemDefinition.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/ValueItem.h"

#include <algorithm>
#include <limits>
#include <map>
#include <sstream>
#include <string>
#include <vector>

namespace smtk
{
namespace simulation
{
namespace truchas
{

MaterialAttributeUtils::MaterialAttributeUtils()
{
  m_skipNames = { "name", "fluid" };
}

bool MaterialAttributeUtils::configureNewAttribute(smtk::attribute::AttributePtr att) const
{
  // Set the forceRequired flag on certain phase properties
  auto phasesGroup = att->findGroup("phases");
  for (std::size_t i = 0; i < phasesGroup->numberOfItemsPerGroup(); ++i)
  {
    smtk::attribute::ItemPtr item = phasesGroup->item(i);
    if ((item->name() == "name") && !item->isValid())
    {
      std::dynamic_pointer_cast<smtk::attribute::StringItem>(item)->setValue("Phase 1");
    }

    if (m_skipNames.count(item->name()) == 0)
    {
      item->setIsEnabled(true); // needed for validity checking
      item->setForceRequired(true);
    }
  }

  return true;
}

bool MaterialAttributeUtils::configureCopiedAttribute(smtk::attribute::AttributePtr att) const
{
  auto phasesGroup = att->findGroup("phases");
  if (phasesGroup->numberOfGroups() == 1)
  {
    return this->configureNewAttribute(att);
  }

  // For multiphase, make a map of shared property vs enabled state
  std::map<std::string, bool> sharedNameMap;
  auto sharedGroup = att->findGroup("shared-properties");
  for (std::size_t i = 0; i < sharedGroup->numberOfItemsPerGroup(); ++i)
  {
    smtk::attribute::ItemPtr item = sharedGroup->item(i);
    sharedNameMap[item->name()] = item->isEnabled();
  }

  // Check all properties in all phases
  for (std::size_t phase = 0; phase < phasesGroup->numberOfGroups(); ++phase)
  {
    for (std::size_t i = 0; i < phasesGroup->numberOfItemsPerGroup(); ++i)
    {
      smtk::attribute::ItemPtr item = phasesGroup->item(phase, i);

      auto findIter = sharedNameMap.find(item->name());
      if (findIter == sharedNameMap.end())
      {
        continue;
      }

      // Set the forceRequired flag by default
      item->setForceRequired(true);
      bool isValid = item->isValid();

      bool sharedEnabled = findIter->second;
      item->setForceRequired(!sharedEnabled);

      bool enabled = isValid && item->isOptional();
      item->setIsEnabled(enabled);
    }
  }

  return true;
}

bool MaterialAttributeUtils::addPhase(smtk::attribute::AttributePtr att) const
{
  auto phasesGroup = att->findGroup("phases");
  auto transitionsGroup = att->findGroup("transitions");
  auto sharedGroup = att->findGroup("shared-properties");

  phasesGroup->appendGroup();
  transitionsGroup->appendGroup();

  if (phasesGroup->numberOfGroups() == 2)
  {
    // Copy valid phase 1 items to shared-properties
    for (std::size_t i = 0; i < phasesGroup->numberOfItemsPerGroup(); ++i)
    {
      smtk::attribute::ItemPtr phaseItem = phasesGroup->item(i);

      if (m_skipNames.count(phaseItem->name()) == 1)
      {
        continue;
      }

      // Set the forceRequired flag by default
      phaseItem->setForceRequired(true);

      // If not valid, leave forceRequired and do not copy to shared
      if (!phaseItem->isValid())
      {
        continue;
      }

      auto sharedItem = sharedGroup->find(phaseItem->name());
      if (sharedItem == nullptr)
      {
        continue;
      }

      // Copy phase 1 item to shared item
      sharedItem->setIsEnabled(true);
      smtk::attribute::ConstItemPtr constPhaseItem = phasesGroup->item(i);
      if (!sharedItem->assign(constPhaseItem, 0))
      {
        std::cerr << "Failed to assign shared item" << sharedItem->name() << std::endl;
        continue;
      }

      // Unset the phase item, making it invalid and thus not enabled
      auto valItem = std::dynamic_pointer_cast<smtk::attribute::ValueItem>(phaseItem);
      if (valItem != nullptr)
      {
        valItem->unset(0);
      }

      // Clear forceRequired flag on the phase item
      phaseItem->setForceRequired(false);
      phaseItem->setIsEnabled(false);
    } // for (i)
  }   // if (2 phases)

  // Set the forceRequired state for each item in the new phase
  std::size_t newPhaseNumber = phasesGroup->numberOfGroups() - 1;
  for (std::size_t i = 0; i < phasesGroup->numberOfItemsPerGroup(); ++i)
  {
    smtk::attribute::ItemPtr phaseItem = phasesGroup->item(newPhaseNumber, i);
    if (m_skipNames.find(phaseItem->name()) != m_skipNames.end())
    {
      continue;
    }

    auto sharedItem = sharedGroup->find(phaseItem->name());
    bool required = sharedItem == nullptr || !sharedItem->isEnabled();
    phaseItem->setForceRequired(required);
    phaseItem->setIsEnabled(required);
  }

  return true;
}

bool MaterialAttributeUtils::removePhase(smtk::attribute::AttributePtr att, std::size_t phase) const
{
  auto phasesGroup = att->findGroup("phases");
  auto transitionsGroup = att->findGroup("transitions");

  phasesGroup->removeGroup(phase);
  std::size_t transition = phase == 0 ? 0 : phase - 1;
  transitionsGroup->removeGroup(transition);

  if (phasesGroup->numberOfGroups() > 1)
  {
    return true;
  }

  // If only 1 phase left
  //   - make all items non-optional
  //   - copy any shared item values to phase 1
  auto sharedGroup = att->findGroup("shared-properties");

  for (std::size_t i = 0; i < phasesGroup->numberOfItemsPerGroup(); ++i)
  {
    smtk::attribute::ItemPtr item = phasesGroup->item(i);

    if (m_skipNames.count(item->name()) == 1)
    {
      continue;
    }

    item->setForceRequired(true);
    if (item->isValid())
    {
      continue;
    }

    // For nonvalid items, see if there is a valid shared item we can copy
    smtk::attribute::ConstItemPtr sharedItem = sharedGroup->find(item->name());
    if ((sharedItem != nullptr) && sharedItem->isEnabled() && sharedItem->isValid())
    {
      item->assign(sharedItem);
    }
  } // for (item)

  return true;
}

bool MaterialAttributeUtils::enableSharedItem(
  smtk::attribute::ItemPtr sharedItem, bool enabled) const
{
  // sharedItem->setIsEnabled(enabled);
  if (m_skipNames.count(sharedItem->name()) == 1)
  {
    std::cerr << "Unexpected shared item " << sharedItem->name() << std::endl;
    return true;
  }

  auto phasesGroup = sharedItem->attribute()->findGroup("phases");

  // Update required state for all phase items of same name
  bool required = !enabled;
  std::string propName = sharedItem->name();
  for (std::size_t i = 0; i < phasesGroup->numberOfGroups(); ++i)
  {
    auto propItem = phasesGroup->find(i, propName);
    propItem->setForceRequired(required);

    // Set propItem enabled before calling isValid()
    propItem->setIsEnabled(true);

    // Now decide whether to keep it enabled
    bool propEnabled = propItem->isOptional() && propItem->isValid();
    propItem->setIsEnabled(propEnabled);
  }
  return true;
}

bool MaterialAttributeUtils::isValid(smtk::attribute::AttributePtr att) const
{
  // Call overloaded method for all categories
  std::set<std::string> categories = { "Heat Transfer", "Enclosure Radiation", "Induction Heating",
    "Fluid Flow", "Solid Mechanics" };
  std::string reason; // not used
  return this->isValid(att, categories, reason);
}

bool MaterialAttributeUtils::isValid(smtk::attribute::AttributePtr att,
  const std::set<std::string>& categories, std::string& reason) const
{
  int version = att->definition()->version();
  if (version < 1)
  {
    // We're not checking validity for older versions
    return true;
  }
  reason = "";

  // Apply the default isValid logic
  if (!att->isValid(categories))
  {
    reason = "attribute contains one or more invalid items";
    // std::cerr << "NOT valid: " << reason << std::endl;
    return false;
  }

  // Check that transition temperatures are monotonically increasing
  auto transitionsGroup = att->findGroup("transitions");
  double minTemp = std::numeric_limits<double>::lowest();
  double maxTemp = minTemp;
  for (std::size_t i = 0; i < transitionsGroup->numberOfGroups(); ++i)
  {
    if (!this->isTransitionValid(transitionsGroup, i, minTemp, maxTemp, reason))
    {
      // std::cerr << "NOT valid: " << reason << std::endl;
      return false;
    }
  }

  // std::cerr << "VALID" << std::endl;
  return true;
}

bool MaterialAttributeUtils::isPhaseValid(smtk::attribute::GroupItemPtr phasesGroup,
  std::size_t element, const std::set<std::string>& categories, std::string& reason) const
{
  for (std::size_t i = 0; i < phasesGroup->numberOfItemsPerGroup(); ++i)
  {
    smtk::attribute::ItemPtr item = phasesGroup->item(element, i);
    if (!item->isEnabled())
    {
      continue;
    }

    // Special case: name not used in phase 0
    if ((element == 0) && (item->name() == "name"))
    {
      continue;
    }

    if (!item->isValid(categories))
    {
      std::stringstream ss;
      ss << "invalid " << item->name() << " property in material attribute "
         << phasesGroup->attribute()->name() << ", phase" << element;
      reason = ss.str();
      return false;
    }
  } // for (i)

  return true;
}

bool MaterialAttributeUtils::isTransitionValid(smtk::attribute::GroupItemPtr transitionsGroup,
  std::size_t element, double& minTemp, double& maxTemp, std::string& reason) const
{
  std::stringstream ss; // if needed

  // Get transition info as either lower/upper or solid fraction table
  auto specItem = transitionsGroup->findAs<smtk::attribute::StringItem>("transition-spec");
  std::string spec = specItem->value();
  if (spec == "two-temp")
  {
    auto loItem = specItem->findChild(
      "lower-transition-temperature", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE);
    double loTemp = std::dynamic_pointer_cast<smtk::attribute::DoubleItem>(loItem)->value();
    if (loTemp <= maxTemp)
    {
      ss << "transition " << element << " low temp " << loTemp
         << " is not greater than previous max temp " << maxTemp;
      reason = ss.str();
      return false;
    }

    auto hiItem = specItem->findChild(
      "upper-transition-temperature", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE);
    double hiTemp = std::dynamic_pointer_cast<smtk::attribute::DoubleItem>(hiItem)->value();
    if (hiTemp <= loTemp)
    {
      ss << "transition " << element << " high temp " << hiTemp << " is not greater than low temp "
         << loTemp;
      reason = ss.str();
      return false;
    }

    // Update min/max and we're done
    minTemp = loTemp;
    maxTemp = hiTemp;
    return true;
  }
  else if (spec == "table")
  {
    auto item =
      specItem->findChild("solid-fraction-table", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE);
    auto tableItem = std::dynamic_pointer_cast<smtk::attribute::DoubleItem>(item);
    if (!tableItem->isSet())
    {
      ss << "transition " << element << " solid fraction table is not assigned";
      reason = ss.str();
      return false;
    }
    auto expressionItem = tableItem->expression();
    auto dataGroup = expressionItem->findGroup("tabular-data");
    std::size_t numRows = dataGroup->numberOfGroups();

    auto tempItem = dataGroup->findAs<smtk::attribute::DoubleItem>("X");

    double loTemp = tempItem->value(0);
    if (loTemp <= maxTemp)
    {
      ss << "transition " << element << " low temp " << loTemp
         << " is not greater than previous max temp " << maxTemp;
      reason = ss.str();
      return false;
    }

    double hiTemp = tempItem->value(numRows - 1);
    if (hiTemp <= loTemp)
    {
      ss << "transition " << element << " high temp " << hiTemp << " is not greater than low temp "
         << loTemp;
      reason = ss.str();
      return false;
    }

    bool tempDirection;
    if (!this->isMonotonic(tempItem, tempDirection))
    {
      ss << "transition " << element << " temperature values are not monotonically ordered";
      reason = ss.str();
      return false;
    }

    auto fractionItem = dataGroup->findAs<smtk::attribute::DoubleItem>("Value");
    bool fractionDirection;
    if (!this->isMonotonic(fractionItem, fractionDirection))
    {
      ss << "transition " << element << " solid-fraction values are not monotonically ordered";
      reason = ss.str();
      return false;
    }

    if (tempDirection == fractionDirection)
    {
      ss << "transition " << element << " temperature and solid-fraction values are both "
         << (tempDirection ? "increasing" : "decreasing");
      reason = ss.str();
      return false;
    }

    // Update min/max and we're done
    minTemp = loTemp;
    maxTemp = hiTemp;
    return true;
  }

  // (else)
  ss << "unrecognized transition-spec type \"" << spec << "\"";
  reason = ss.str();
  return false;
}

bool MaterialAttributeUtils::isMonotonic(smtk::attribute::DoubleItemPtr item, bool& direction) const
{
  auto ascendIter = std::adjacent_find(item->begin(), item->end(), std::greater<double>());
  if (ascendIter == item->end())
  {
    direction = true;
    return true;
  }

  auto descendIter = std::adjacent_find(item->begin(), item->end(), std::less<double>());
  if (descendIter == item->end())
  {
    direction = false;
    return true;
  }

  return false;
}
}
}
}
