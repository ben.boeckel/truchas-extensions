//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include <pybind11/pybind11.h>

namespace py = pybind11;

// template <typename T, typename... Args>
// using PySharedPtrClass = py::class_<T, std::shared_ptr<T>, Args...>;

#include "PybindMaterialAttributeUtils.h"

// PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);

PYBIND11_MODULE(_smtkPybindTruchasSimulation, m)
{
  m.doc() = "<description>";
  py::module smtk = m.def_submodule("smtk", "<description>");
  py::module truchas = smtk.def_submodule("truchas", "<description>");

  py::class_< smtk::simulation::truchas::MaterialAttributeUtils > smtk_truchas_MaterialAttributeUtils =
    pybind11_init_smtk_truchas_MaterialAttributeUtils(truchas);
}
