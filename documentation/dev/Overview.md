# Development Overview

The files used to customize CMB modelbuilder for Truchas are located under the top-level `simulation-workflows/` folder. This folder contains two files that serve as entry points.

* `Truchas.sbt` has the entry point for the Truchas "template" files, which specify the data that are displayed in the modelbuilder "Attribute Editor" panel for users to specify Truchas simulations.
* `Truchas.py` has the entry point for the "export" scripts, which write Truchas input files based on the data in the Attribute Editor. Internally, `Truchas.py` implements a CMB "operation", which is a CMB class that handles data transfer between modelbuilder client and server processes. The details of CMB operations are not important for developing export scripts. For practical purposes, the operation code in `Truchas.py` calls a function `ExportCMB()` in the same file; the `ExportCMB()` function can be considered the entry point for generating Truchas input files.


## Templates

Todo


## Writers

The detailed code for writing Truchas input files can be found in the folder at `simulation-workflows/internal/writer/`. When an export action is invoked by the CMB users, the operation code in `Truchas.py` calls the `ExportCMB()` function, which in turn initializes one of two writer classes, either:

* `GenreWriter` in the `simulation-workflows/intenal/writer/genrewriter.py` file. This is used when Genre input files are selected in the CMB export dialog.
* `TruchasWriter` in the `simulation-workflows/intenal/writer/truchaswriter.py` file. This is used when Truchas input files are selected in the CMB export dialog.


### Genre Writer

Todo

### Truchas Writer

The `TruchasWriter` class is used to generate the Truchas input file based on attribute data in CMB modelbuilder. This class inherits two classes:

* `BaseWriter`, in file `basewriter.py`, is a base class with shared member data and methods.
* `MaterialWriter`, in file `materialwriter.py`, is a mixin class, providing methods specifically for writing Truchas material namelists (MATERIAL, MATERIAL_SYSTEM, PHASE). Without going into details here, the `TruchasWriter` writes the material namelists by calling the `MaterialWriter._write_physical_materials()` method. The `MaterialWriter` class is described in `MaterialWriter.md`.
* For typical simulations, functions are used to specify some material properties. The FUNCTION namelists are written as part of the main export processing in the `TruchasWriter` class.
