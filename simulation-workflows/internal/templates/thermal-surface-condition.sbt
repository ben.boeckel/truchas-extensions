<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Heat Transfer</Cat>
  </Categories>
  <!-- Attribute definitions for thermal surface conditions-->
  <Definitions>
    <!-- Boundary condition definitions-->
    <AttDef Type="ht.boundary" BaseType="" Abstract="true" Version="0">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="ht.boundary.temperature" Label="Temperature" BaseType="ht.boundary" Unique="true" RootName="Temperature" Version="0">
      <ItemDefinitions>
        <Double Name="temp" Label="Temperature">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>fn.ht.temperature</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ht.boundary.flux" Label="Flux" BaseType="ht.boundary" Unique="true" RootName="Flux" Version="0">
      <ItemDefinitions>
        <Double Name="flux" Label="Heat Flux">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>fn.ht.flux</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ht.boundary.htc" Label="HTC" BaseType="ht.boundary" Unique="true" RootName="HTC" Version="0">
      <ItemDefinitions>
        <Double Name="htc" Label="Heat Transfer Coefficient">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>fn.ht.htc</ExpressionType>
        </Double>
        <Double Name="ambient-temp" Label="Ambient Temperature">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>fn.ht.ambient-temperature</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ht.boundary.radiation" Label="Radiation" BaseType="ht.boundary" Unique="true" RootName="Radiation" Version="0">
      <ItemDefinitions>
        <Double Name="emissivity" Label="Emissivity">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>fn.ht.emissivity</ExpressionType>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
        <Double Name="ambient-temp" Label="Ambient Temperature">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>fn.ht.ambient-temperature</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <!-- Interface condition definitions-->
    <AttDef Type="ht.interface" BaseType="" Abstract="true" Version="0">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="ht.interface.interface-htc" Label="HTC" BaseType="ht.interface" Unique="true" RootName="ifHTC" Version="0">
      <ItemDefinitions>
        <Double Name="htc" Label="Heat Transfer Coefficient">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>fn.ht.htc</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ht.interface.gap-radiation" Label="Gap Radiation" BaseType="ht.interface" Unique="true" RootName="ifRadiation" Version="0">
      <ItemDefinitions>
        <Double Name="emissivity" Label="Emissivity">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ExpressionType>fn.ht.emissivity</ExpressionType>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
