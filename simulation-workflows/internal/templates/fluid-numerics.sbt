<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Fluid Flow</Cat>
  </Categories>
  <Definitions>
    <!-- Fluid Solver Settings-->
    <AttDef Type="fluid-solver" Label="Solver" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="flow-numerics" Label="Flow Numerics">
          <ItemDefinitions>
            <String Name="discrete-ops-type" Label="Discrete_Ops_type">
              <BriefDescription>Numerical reconstruction method for estimating face-centered
spatial graients and values of discrete cell-centered data.</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Default">default</Value>
                <Value Enum="Ortho">ortho</Value>
                <Value Enum="Non-ortho">nonortho</Value>
              </DiscreteInfo>
              <Categories>
                <Cat>Fluid Flow</Cat>
              </Categories>
            </String>
            <Double Name="viscous-number" Label="Viscous Number">
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Int Name="volume-track-subcycles" Label="Volume Track Subcycles">
              <BriefDescription>Volume tracking time step specification, expressed as a
multiplicative factor relative to the time step</BriefDescription>
              <DefaultValue>2</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">20</Max>
              </RangeInfo>
            </Int>
            <Double Name="body-force-implicitness" Label="Body Force Implicitness" AdvanceLevel="1">
              <BriefDescription>Degree of time implicitness used for the body-forces in the
discrete form of the momentum equations</BriefDescription>
              <DefaultValue>0.5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
                <Max Inclusive="true">1.0</Max>
              </RangeInfo>
            </Double>
            <Group Name="mass-limiter" Label="Mass Limiter" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false">
              <BriefDescription>Used to avoid the generation of excessively large velocities
in partially-filled fluid-void cells where the fluid mass
approaches the cutoff volume-fraction</BriefDescription>
              <ItemDefinitions>
                <Double Name="mass-limiter-cutoff" Label="Mass Limiter Cutoff">
                  <BriefDescription>Used to specify the the value of a material cell volume
fraction below which mass is ignored</BriefDescription>
                  <DefaultValue>1.0e-5</DefaultValue>
                  <RangeInfo>
                    <Max Inclusive="false">1.0</Max>
                  </RangeInfo>
                </Double>
              </ItemDefinitions>
            </Group>
            <Double Name="mechanical-energy-bound" Label="Mechanical Energy Bound" AdvanceLevel="1">
              <BriefDescription>Control option for applying the body force at cell faces
during the velocity correction step</BriefDescription>
              <Categories>
                <Cat>Fluid Flow</Cat>
              </Categories>
              <ChildrenDefinitions>
                <Double Name="value" Label="Value"/>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="infinity"/>
                <Structure>
                  <Value Enum="value"/>
                  <Items>
                    <Item>value</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </Double>
            <Double Name="momentum-solidify-implicitness" Label="Momentum Solidify Implicitness" AdvanceLevel="1">
              <BriefDescription>Degree of time implicitness used for the velocity field in the
treatment of the momentum deposition associated with solidification</BriefDescription>
              <DefaultValue>0.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
                <Max Inclusive="true">1.0</Max>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="projection-linear-solver" Label="Projection Linear Solver">
          <ItemDefinitions>
            <String Name="method" Label="Method">
              <BriefDescription>Algorithm used for solution of linear systems</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value>fgmres</Value>
              </DiscreteInfo>
            </String>
            <String Name="preconditioning-method" Label="Preconditioning Method">
              <BriefDescription>Algorithm used to precondition the Krylov iteration</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value>ssor</Value>
              </DiscreteInfo>
            </String>
            <Int Name="preconditioning-steps" Label="Preconditioning Steps">
              <BriefDescription>Number of passes to be performed in Jacobi or SSOR preconditioning</BriefDescription>
              <DefaultValue>2</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Int>
            <Double Name="relaxation-parameter" Label="Relaxation Parameter">
              <BriefDescription>Relaxation parameter used in Jacobi and SSOR preconditioning</BriefDescription>
              <DefaultValue>1.4</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
                <Max Inclusive="false">2.0</Max>
              </RangeInfo>
            </Double>
            <String Name="stopping-criterion" Label="Stopping Criterion">
              <BriefDescription>Test used to estimate error and determine when convergence
has been reached</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value>||r||</Value>
                <Value>||r||/||b||</Value>
              </DiscreteInfo>
            </String>
            <Double Name="convergence-criterion" Label="Convergence Criterion">
              <BriefDescription>Value of error estimate used to determine when convergence
has been reached</BriefDescription>
              <DefaultValue>1.0e-8</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
                <Max Inclusive="false">0.1</Max>
              </RangeInfo>
            </Double>
            <Int Name="krylov-vectors" Label="Krylov Vectors" AdvanceLevel="1">
              <BriefDescription>Number of vectors used by GMRES and FGMRES in orthogonalization</BriefDescription>
              <DefaultValue>50</DefaultValue>
              <RangeInfo>
                <Main Inclusive="false">0</Main>
              </RangeInfo>
            </Int>
            <String Name="output-mode" Label="Output Mode" AdvanceLevel="1">
              <BriefDescription>Controls verbosity of linear solver</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value>none</Value>
                <Value>errors</Value>
                <Value>errors+warnings</Value>
                <Value>summary</Value>
                <Value>iterates</Value>
                <Value>full</Value>
              </DiscreteInfo>
            </String>
          </ItemDefinitions>
        </Group>
        <Group Name="viscous-flow-model" Label="Viscous Flow Model" Optional="true" IsEnabledByDefault="false">
          <ItemDefinitions>
            <Double Name="viscous-implicitness" Label="Viscous Implicitness">
              <BriefDescription>Degree of time implicitness used in velocity field calculations</BriefDescription>
              <DefaultValue>0.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
                <Max Inclusive="true">1.0</Max>
              </RangeInfo>
            </Double>
            <Group Name="viscous-linear-solver" Label="Viscous Linear Solver">
              <ItemDefinitions>
                <String Name="method" Label="Method">
                  <BriefDescription>Algorithm used for solution of linear systems</BriefDescription>
                  <DiscreteInfo DefaultIndex="0">
                    <Value>fgmres</Value>
                  </DiscreteInfo>
                </String>
                <String Name="preconditioning-method" Label="Preconditioning Method">
                  <BriefDescription>Algorithm used to precondition the Krylov iteration</BriefDescription>
                  <DiscreteInfo DefaultIndex="0">
                    <Value>diagonal</Value>
                  </DiscreteInfo>
                </String>
                <Int Name="preconditioning-steps" Label="Preconditioning Steps">
                  <BriefDescription>Number of passes to be performed in Jacobi or SSOR preconditioning</BriefDescription>
                  <DefaultValue>2</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="false">0</Min>
                  </RangeInfo>
                </Int>
                <Double Name="relaxation-parameter" Label="Relaxation Parameter">
                  <BriefDescription>Relaxation parameter used in Jacobi and SSOR preconditioning</BriefDescription>
                  <DefaultValue>1.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="false">0.0</Min>
                    <Max Inclusive="false">2.0</Max>
                  </RangeInfo>
                </Double>
                <String Name="stopping-criterion" Label="Stopping Criterion">
                  <BriefDescription>Test used to estimate error and determine when convergence
has been reached</BriefDescription>
                  <DiscreteInfo DefaultIndex="1">
                    <Value>||r||</Value>
                    <Value>||r||/||b||</Value>
                  </DiscreteInfo>
                </String>
                <Double Name="convergence-criterion" Label="Convergence Criterion">
                  <BriefDescription>Value of error estimate used to determine when convergence
has been reached</BriefDescription>
                  <DefaultValue>1.0e-8</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="false">0.0</Min>
                    <Max Inclusive="false">0.1</Max>
                  </RangeInfo>
                </Double>
                <Int Name="krylov-vectors" Label="Krylov Vectors" AdvanceLevel="1">
                  <BriefDescription>Number of vectors used by GMRES and FGMRES in orthogonalization</BriefDescription>
                  <DefaultValue>50</DefaultValue>
                  <RangeInfo>
                    <Main Inclusive="false">0</Main>
                  </RangeInfo>
                </Int>
                <String Name="output-mode" Label="Output Mode" AdvanceLevel="1">
                  <BriefDescription>Controls verbosity of linear solver</BriefDescription>
                  <DiscreteInfo DefaultIndex="0">
                    <Value>none</Value>
                    <Value>errors</Value>
                    <Value>errors+warnings</Value>
                    <Value>summary</Value>
                    <Value>iterates</Value>
                    <Value>full</Value>
                  </DiscreteInfo>
                </String>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
