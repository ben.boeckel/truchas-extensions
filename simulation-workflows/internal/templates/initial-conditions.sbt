<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
    <Cat>Solid Mechanics</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="TemperatureInitialCondition" Label="Temperature" Unique="true">
      <AssociationsDef Name="DSSourceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="temperature" Label="Temperature" NumberOfRequiredValues="1">
          <BriefDescription>Initial temperature of the material body</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <ExpressionType>fn.initial-condition.temperature</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="VelocityInitialCondition" Label="Velocity" Unique="true">
      <AssociationsDef Name="DSSourceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="velocity" Label="Velocity" NumberOfRequiredValues="3">
          <BriefDescription>Initial velocity of the material body</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
