<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Views>
    <View Type="Group" Title="Fluid Flow" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="FF Boundary Conditions" />
        <View Title="FF Solver" />
      </Views>
    </View>

    <View Type="Attribute" Title="FF Boundary Conditions" Label="Boundary Conditions">
      <AttributeTypes>
        <Att Type="boundary-condition"/>
      </AttributeTypes>
    </View>


    <View Type="Instanced" Title="FF Solver" Label="Solver">
      <InstancedAttributes>
        <Att Type="fluid-solver" Name="fluid-solver" />
      </InstancedAttributes>
    </View>

  </Views>
</SMTK_AttributeSystem>
